# 线性模型

## 1. 线性模型概念

1、线性模型

所谓线性模型，就是把事物的特征属性，按照线性组合的方式构造出假设函数(Hypothesis)并加以训练的一种算法模型。

2、线性模型一般式

hθ(x)=θT⋅x+b

hθ(x)=θ1⋅x1+θ2⋅x2+⋯+θn⋅xn+b

很多入门教程也会写做 *h(x)=wT⋅x+b*

3、权重参数

这里的θ或者w作用都是当做权重参数， θi⋅xi中的θi表示特征 xi的重要程度。

举例：hθ(x)=0.03⋅x1+0.8⋅x2+0.2

简言之，对于这个假设函数，事物的特征x2比x1更重要，更有助于提高算法模型的最终预测准确率。

## 2. LR算法直观原理

1、线性模型中最为经典的就数线性归回(Linear Regression)算法。

以Ng课程的房价预测为例，样本只有一个特征x：房屋面积。价格price为数据样本标签的y。

![](./src/images/figure_8.png)

放在XoY坐标轴上，如下图：

![9](./src/images/figure_9.png)

经过训练集的数据训练，LR算法最终学习得到一个假设函数， hθ(x)=θT⋅x+b， 就是图中的直线。有了这个函数，随便输入一个房屋的面积值，就可以得到一个预测价格，以供业务参考。

2、以上便是LR最直观的原理解释，就是通过训练得到一组合适的权重参数而已。但这里只有面积这一个特征，实际中会有n多个特征供以选择，很可能无法图形化展示出来。而如何选择特征就涉及到特征工程了。

3、算法步骤

(1) 初始化特征集

首先设置偏置项bias，即假设函数中 b b b。一般性做法是在首(尾)加入一列特征值全为1的列，如下表示例：

| b    | x1   | x2   |
| ---- | ---- | ---- |
| 1    | 3    | 10   |
| 1    | 6    | 11   |
| 1    | 8    | 10   |

(2) 初始化参数集θ

θ=(θ0,θ1,θ2)=(0,0,0)

θ一般初始化全为0，若初始化为随机值对梯度下降算法收敛速度提升与否，暂时还没验证

(3) 构造$h_\theta(x) = \theta_0·1 + \theta_1·x_1 + \theta_2·x_2 $

(4) 结合hθ和标签y，使用梯度下降算法收敛时，得到一组可行参数 θ(θ0,θ1,θ2)

## 3. Python代码实现算法

1、构造特征集和样本标签集

```python
def Create_Train_Set(data_set_path):

    #get train_data_set
    train_data = Get_Data_Set(path = data_set_path + 'ds1_train.csv')
    #create train_data label y
    train_data_y = train_data.iloc[:, -1].values.reshape((len(train_data),1))

    #create train_data features and  insert bias into features
    train_data_X = train_data.iloc[:, :-1]
    train_data_X.insert(0,'bias',1)
    train_data_X = train_data_X.values
    
    return train_data_X, train_data_y
```

2、构造初始化θ

```python
def Linear_Regression(data_set_path):

    #get train data
    train_data_X, train_data_y = Create_Train_Set(data_set_path)
    #initialize theta 
    theta = np.zeros((train_data_X.shape[1],1))

print(theta)
```

3、调用梯度下降算法

按照梯度下降算法的原理，迭代更新θ参数

下图中的alpha，iters属于根据主观意愿选择数值，就是通常意义上的调参

![10](./src/images/figure_10.png)

代码：

```python
def Gradient_Descent(X,y,theta,alpha, iters):

    for _ in range(iters):
        theta = theta - (X.T @ (X@theta - y) ) * alpha / len(X)

    return theta
```

θ迭代6000次后的结果：

![11](./src/images/figure_11.png)

4、对于LR来说，一般得到权重参数θ后，任务就基本完成了。剩余的就是对于测试集中的数据做特征工程，以及代入Hypothesis函数得出预测值等任务了

5、完整代码

```python
import pandas as pd
import numpy as np

def Get_Data_Set(path):
    data = pd.read_csv(path)
    return data

def Create_Train_Set(data_set_path):

    #get initial train data
    train_data = Get_Data_Set(path = data_set_path + 'ds1_train.csv')
    #create train_data label y
    train_data_y = train_data.iloc[:, -1].values.reshape((len(train_data),1))

    #create train_data features and  insert bias into features
    train_data_X = train_data.iloc[:, :-1]
    train_data_X.insert(0,'bias',1)
    train_data_X = train_data_X.values

    return train_data_X, train_data_y

def Hypothesis(theta,X):
    """hypothesis function"""
    return X @ theta

def Cost_Function(theta,X,y):
    """cost function"""

    cost = np.power(Hypothesis(theta,X) - y, 2)
    return np.sum(cost) / 2

def Gradient_Descent(X,y,theta,alpha, iters):
    """gradient descent algorithm"""
    for _ in range(iters):
        theta = theta - (X.T @ (X@theta - y) ) * alpha / len(X)

    return theta

def Linear_Regression(data_set_path):

    #get train data
    train_data_X, train_data_y = Create_Train_Set(data_set_path)
    #initialize theta
    theta = np.zeros((train_data_X.shape[1],1))

    #call Gradient Descent,
    #alpha = 0.0001, iters = 6000
    theta = Gradient_Descent(train_data_X,train_data_y,theta,0.0001,6000)


if __name__ == '__main__':
    data_set_path = './data/PS1/'
    Linear_Regression(data_set_path)
```