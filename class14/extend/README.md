# 14. 房价预测

## 1. 什么是线性回归

我们首先用弄清楚什么是线性，什么是非线性。

- 线性：两个变量之间的关系**是**一次函数关系的——图象**是直线**，叫做线性。

  **注意：题目的线性是指广义的线性，也就是数据与数据之间的关系。**

- 非线性：两个变量之间的关系**不是**一次函数关系的——图象**不是直线**，叫做非线性。

相信通过以上两个概念大家已经很清楚了，其次我们经常说的回归回归到底是什么意思呢。

- 回归：人们在测量事物的时候因为客观条件所限，求得的都是测量值，而不是事物真实的值，为了能够得到真实值，无限次的进行测量，最后通过这些测量数据计算**回归到真实值**，这就是回归的由来。

通俗的说就是用一个函数去逼近这个真实值，那又有人问了，线性回归不是用来做预测吗？是的，通过大量的数据我们是可以预测到**真实值**的。

## 2. 线性回归要解决什么问题

对大量的观测数据进行处理，从而得到比较符合事物内部规律的数学表达式。也就是说寻找到数据与数据之间的规律所在，从而就可以模拟出结果，也就是对结果进行预测。解决的就是通过已知的数据得到未知的结果。例如：对房价的预测、判断信用评价、电影票房预估等。

## 3. 线性回归的一般模型

![image](src/images/figure_1.png)

大家看上面图片，图片上有很多个小点点，通过这些小点点我们很难预测当x值=某个值时，y的值是多少，我们无法得知，所以，数学家是很聪明的，是否能够找到一条直线来描述这些点的趋势或者分布呢？答案是肯定的。相信大家在学校的时候都学过这样的直线，只是当时不知道这个方程在现实中是可以用来预测很多事物的。

那么问题来了，什么是模型呢？先来看看下面这幅图。

![image](src/images/figure_2.png)

假设数据就是**x**，结果是**y**，那中间的模型其实就是一个**方程**，这是一种片面的解释，但有助于我们去理解模型到底是个什么东西。以前在学校的时候总是不理解数学建模比赛到底在做些什么，现在理解了，是从题目给的数据中找到数据与数据之间的关系，建立数学方程模型，得到结果解决现实问题。其实是和机器学习中的模型是一样的意思。那么线性回归的一般模型是什么呢？

![image](src/images/figure_3.png)

模型神秘的面纱已经被我们揭开了，就是以上这个公式，不要被公式吓到，只要知道模型长什么样就行了。假设i=0，表示的是一元一次方程，是穿过坐标系中原点的一条直线，以此类推。

## 4. 如何使用模型

我们知道x是已知条件，通过公式求出y。已知条件其实就是我们的数据，以预测房价的案例来说明：

![image](src/images/figure_4.png)

上图给出的是某个地区房价的一些相关信息，有日期、房间数、建筑面积、房屋评分等特征，表里头的数据就是我们要的x1、x2、x3…….... 自然的表中的price列就是房屋的价格，也就是y。现在需要求的就是theta的值了，后续步骤都需要依赖计算机来训练求解。

## 5. 模型计算

当然，这些计算虽然复杂，但python库中有现成的函数直接调用就可以求解。我们为了理解内部的计算原理，就需要一步一步的来剖析计算过程。

为了容易理解模型，假设该模型是一元一次函数，我们把一组数据x和y带入模型中，会得到如下图所示线段。

![image](src/images/figure_5.png)

## 6. 实例

题目描述：从给定的房屋基本信息以及房屋销售信息等，建立一个回归模型预测房屋的销售价格。文件见`/src`目录

- **数据说明**：
   数据主要包括2014年5月至2015年5月美国King County的房屋销售价格以及房屋的基本信息。
   数据分为训练数据和测试数据，分别保存在kc_train.csv和kc_test.csv两个文件中。
   其中训练数据主要包括10000条记录，14个字段，主要字段说明如下：
   第一列“销售日期”：2014年5月到2015年5月房屋出售时的日期
   第二列“销售价格”：房屋交易价格，单位为美元，是目标预测值
   第三列“卧室数”：房屋中的卧室数目
   第四列“浴室数”：房屋中的浴室数目
   第五列“房屋面积”：房屋里的生活面积
   第六列“停车面积”：停车坪的面积
   第七列“楼层数”：房屋的楼层数
   第八列“房屋评分”：King County房屋评分系统对房屋的总体评分
   第九列“建筑面积”：除了地下室之外的房屋建筑面积
   第十列“地下室面积”：地下室的面积
   第十一列“建筑年份”：房屋建成的年份
   第十二列“修复年份”：房屋上次修复的年份
   第十三列"纬度"：房屋所在纬度
   第十四列“经度”：房屋所在经度

测试数据主要包括3000条记录，13个字段，跟训练数据的不同是测试数据并不包括房屋销售价格，学员需要通过由训练数据所建立的模型以及所给的测试数据，得出测试数据相应的房屋销售价格预测值。

### 6.1 步骤

![image](src/images/figure_6.png)

- 1.选择合适的模型，对模型的好坏进行评估和选择。
- 2.对缺失的值进行补齐操作，可以使用均值的方式补齐数据，使得准确度更高。
- 3.数据的取值一般跟属性有关系，但世界万物的属性是很多的，有些值小，但不代表不重要，所有为了提高预测的准确度，统一数据维度进行计算，方法有特征缩放和归一法等。
- 4.数据处理好之后就可以进行调用模型库进行训练了。
- 5.使用测试数据进行目标函数预测输出，观察结果是否符合预期。或者通过画出对比函数进行结果线条对比。

### 6.2 模型选择

这里我们选择多元线性回归模型。公式如下：选择多元线性回归模型。

![image](src/images/figure_7.png)

y表示我们要求的销售价格，x表示特征值。需要调用sklearn库来进行训练。

### 6.3 环境配置

- python3.5
- numpy库
- pandas库
- matplotlib库进行画图
- seaborn库
- sklearn库

### 6.4 csv数据处理

下载的是两个数据文件，一个是真实数据，一个是测试数据，打开*kc_train.csv*，能够看到第二列是销售价格，而我们要预测的就是销售价格，所以在训练过程中是不需要销售价格的，把第二列删除掉，新建一个csv文件存放销售价格这一列，作为后面的结果对比。

### 6.5 数据处理

首先先读取数据，查看数据是否存在缺失值，然后进行特征缩放统一数据维度。代码如下：(注：最后会给出完整代码)

```python
from __future__ import print_function

# 导入相关python库
import os
import numpy as np
import pandas as pd

#设定随机数种子
np.random.seed(36)

#使用matplotlib库画图
import matplotlib
import seaborn
import matplotlib.pyplot as plot

from sklearn import datasets


#读取数据
df=pd.read_csv('kc_train.csv')
df.columns=['销售日期','销售价格','卧室数','浴室数','房屋面积',
            '停车面积','楼层数','房屋评分','建筑面积','地下室面积',
            '建筑年份','修复年份','纬度','经度']
target=df['销售价格']
df.drop(labels=['销售价格'],inplace=True,axis=1)
housing=df
# housing = pd.read_csv('kc_train.csv')
# target=pd.read_csv('kc_train2.csv')  #销售价格
t=pd.read_csv('kc_test.csv')         #测试数据
t.columns=['销售日期','卧室数','浴室数','房屋面积',
            '停车面积','楼层数','房屋评分','建筑面积','地下室面积',
            '建筑年份','修复年份','纬度','经度']


#特征缩放
from sklearn.preprocessing import MinMaxScaler
minmax_scaler=MinMaxScaler()
minmax_scaler.fit(housing)
#进行内部拟合，内部参数会发生变化
scaler_housing=minmax_scaler.transform(housing)
scaler_housing=pd.DataFrame(scaler_housing,columns=housing.columns)

mm=MinMaxScaler()
mm.fit(t)
scaler_t=mm.transform(t)
scaler_t=pd.DataFrame(scaler_t,columns=t.columns)



#选择基于梯度下降的线性回归模型
from sklearn.linear_model import LinearRegression
LR_reg=LinearRegression()
#进行拟合
LR_reg.fit(scaler_housing,target)


#使用均方误差用于评价模型好坏
from sklearn.metrics import mean_squared_error
preds=LR_reg.predict(scaler_housing)   #输入数据进行预测得到结果
mse=mean_squared_error(preds,target)   #使用均方误差来评价模型好坏，可以输出mse进行查看评价值

#绘图进行比较
plot.figure(figsize=(10,7))       #画布大小
num=100
x=np.arange(1,num+1)              #取100个点进行比较
plot.plot(x,target[:num],label='target')      #目标取值
plot.plot(x,preds[:num],label='preds')        #预测取值
plot.legend(loc='upper right')  #线条显示位置
plot.show()


#输出测试数据
result=LR_reg.predict(scaler_t)
df_result=pd.DataFrame(result)
df_result.to_csv("result.csv")

```
