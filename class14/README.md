# 线性回归

## 1. 线性模型概念

### 1.1 线性模型

所谓线性模型，就是把事物的特征属性，按照线性组合的方式构造出假设函数(Hypothesis)并加以训练的一种算法模型。

### 1.2 线性模型公式

hθ(x)=θT⋅x+b

hθ(x)=θ1⋅x1+θ2⋅x2+⋯+θn⋅xn+b

很多入门教程也会写做 *h(x)=wT⋅x+b*

### 1.3 权重参数

这里的θ或者w作用都是当做权重参数， θi⋅xi中的θi表示特征 xi的重要程度。

举例：hθ(x)=0.03⋅x1+0.8⋅x2+0.2

简言之，对于这个假设函数，事物的特征x2比x1更重要，更有助于提高算法模型的最终预测准确率。

## 2. LR算法直观原理

1、线性模型中最为经典的就数线性归回(Linear Regression)算法。

以Ng课程的房价预测为例，样本只有一个特征x：房屋面积。价格price为数据样本标签的y。

![](./extend/src/images/figure_8.png)

放在XoY坐标轴上，如下图：

![9](./extend/src/images/figure_9.png)

经过训练集的数据训练，LR算法最终学习得到一个假设函数， hθ(x)=θT⋅x+b， 就是图中的直线。有了这个函数，随便输入一个房屋的面积值，就可以得到一个预测价格，以供业务参考。

2、以上便是LR最直观的原理解释，就是通过训练得到一组合适的权重参数而已。但这里只有面积这一个特征，实际中会有n多个特征供以选择，很可能无法图形化展示出来。而如何选择特征就涉及到特征工程了。

## 3. 线性回归的实现

首先导入依赖项，导入 numpy， pandas和 matplotlib。

```bash
pip install numpy 
pip install pandas
pip install matplotlib
```

numpy用于处理矩阵，处理数据时会使用pandas，处理图形时会使用matplotlib。 numpy， pandas和 matplotlib是实现机器学习最重要的库。

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
```

现在让我们加载我们的数据集。我把数据集分成两组，一个用于训练，一个用于测试。你可以从src下载数据集：train_dataset.csv，test_dataset.csv，打开上述文件后，将整个内容复制粘贴到您的 
记事本并保存。

```python
train = pd.read_csv("train_dataset.csv")
test = pd.read_csv("test_dataset.csv")
print(train.head())
```

下面看看数据集的内容

```python
print(train.head())
```

![1](./src/images/figure_1.png)

train_dataset，test_dataset数据集有两个不需要的特征（Unnamed 和 Id），所以我们删除这两列。最后可以看到有一个销售价格。销售价格是我们的预测价格，即 Y，所有其他特征都是输入的 X。 下面附上了一个文件(

[data_description.txt]: ./src/data_description.txt

)，其中详细描述了程序的功能。原始数据集有很多分类，其中有的数据字段中是字符或字符串。现在无法训练机器具有分类的学习模型，所以首先预处理数据，将字符串分类特征转换为数值特征。除了预处理，我还做了很多其他的预处理对于数据集的训练至关重要。现在我们删除两个不需要的列。

```python
train = train.drop(["Unnamed: 0", "Id"], axis = 1)
test = test.drop(["Unnamed: 0", "Id"], axis = 1)
```

下面我们分开X和Y

```python
train_data = train.values
Y = train_data[:, -1].reshape(train_data.shape[0], 1)
X = train_data[:, :-1]
```

```python
test_data = test.values
Y_test = test_data[:, -1].reshape(test_data.shape[0], 1)
X_test = test_data[:, :-1]
```

现在让我们看看我们的数据集的shape。

```python
print("Shape of X_train :", X.shape)
print("Shape of Y_train :", Y.shape)
print("Shape of X_test :", X_test.shape)
print("Shape of Y_test :", Y_test.shape)
```

输出

```bash
Shape of X_train : (1200, 69)
Shape of Y_train : (1200, 1)
Shape of X_test : (258, 69)
Shape of Y_test : (258, 1)
```

可见train数据集中有 1200 栋房屋的数据，每栋房屋有 70 特征。同样，test数据集中有 258 栋房屋。我们知道在线性回归中的预测方程为

y_pred = θ n * n + θ n−1 * n−1 + θ n−2 * n−2 +...+ θ 2 * 2 + θ1 * 1 + θ 0

在python中，我们可以将上面的方程写为矩阵 θ和X的乘法

y_pred = matrix_mul(X,theta)

![2](./src/images/figure_2.png)

现在，要对X和θ进行适当的矩阵乘法，我们需要在X的所有特征前加一列。这样做的原因是，我们乘以θ2与X2 ，θ 1与X1，但不存在X0可与θ0相乘。所以我们在X0的位置加1列。

```python
X = np.vstack((np.ones((X.shape[0], )), X.T)).T
X_test = np.vstack((np.ones((X_test.shape[0], )), X_test.T)).T
```

> 备注：numpy.vstack
>
> numpy.vstack(tup)   [source]
>
> 垂直(行)按顺序堆叠数组。
>
> 这等效于形状(N,)的1-D数组已重塑为(1,N)后沿第一轴进行concatenation。 重建除以vsplit的数组。

现在知道成本函数是误差表示，可通过用实际值减去预测值来找出误差。成本函数将通过上面的公式来计算。为了最小化错误（或成本），我们需要使用梯度下降通过达到局部最小值的方式计算成本值。 

我们在 python 中创建线性回归模型。  

1. 我采用四个参数 X、Y、学习率（即 alpha）和 迭代。Iterations 指定我们要运行多少次循环。  

2. 定义 m 为数据集的大小（当前为 1200）。  

3. theta（θ）是零向量。所以它是一个大小为 (n, 1) 其中 n 是特征的数量。大小为 (70, 1)  

4. 正在运行循环以获取迭代时间。并且在每次迭代中计算上面的 4 个方程。  

5. 将在每次迭代中跟踪我们的cost，维护一个cost_list。 

6. 最后，返回 theta 参数和cost

```python
def model(X, Y, learning_rate, iteration):
    m = Y.size
    theta = np.zeros((X.shape[1], 1))
    cost_list = []
    for i in range(iteration):
        y_pred = np.dot(X, theta)
        cost = (1/(2*m))*np.sum(np.square(y_pred - Y))
        d_theta = (1/m)*np.dot(X.T, y_pred - Y)
        theta = theta - learning_rate*d_theta
        cost_list.append(cost)
        # to print the cost for 10 times
        if(i%(iteration/10) == 0):
            print("Cost is :", cost)
    return theta, cost_list
```

下面调用模型，

```python
iteration = 10000
learning_rate = 0.000000005
theta, cost_list = model(X, Y, learning_rate = learning_rate, iteration =
iteration)
```

结果

```bash
Cost is : 72.37539364066856
Cost is : 0.027904168310316887
Cost is : 0.017251065372144166
Cost is : 0.016355272705548287
Cost is : 0.01615883608753075
Cost is : 0.016040958498450605
Cost is : 0.01594682732375346
Cost is : 0.015867896317230026
Cost is : 0.015800568014785396
Cost is : 0.0157423553064829
```

可以看到成本随着每次迭代而下降。我们还可以绘制一个成本与迭代图。

![3](./src/images/figure_3.png)

我们使用测试数据集上测试模型的准确性。通过1减去误差得到准确度。以下是误差公式：

error = (1/m) * ∑|y_pred−Y|

```python
y_pred = np.dot(X_test, theta)
error = (1/X_test.shape[0])*np.sum(np.abs(y_pred - Y_test))
print("Test error is :", error*100, "%")
print("Test Accuracy is :", (1- error)*100, "%")
```

结果

```bash
Test error is : 12.957658795431648 %
Test Accuracy is : 87.04234120456834 %
```

我们的模型有 87% 的准确率。

​                 
