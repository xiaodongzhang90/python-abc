# 15. 人脸识别

## 1.人脸识别技术原理

人脸识别无非三步或者四部，即三步：人脸检测、特征提取、人脸识别，四步：人脸检测、检测特征点、计算特征特征向量、识别计算。可以把四步中的检测特征点和计算特征向量理解为三步中的特征提取，不同之处是采取的算法不同。从人脸识别开始到目前，各种各样的人脸识别算法层出不穷，近些年，CNN快速发展，基于CNN的人脸识别算法不停刷新人脸识别检测精度。

从上述可以知道，人脸识别是通过检测器和分类器实现的，因此，搭配好检测器和分类器就可以实现人脸识别。（如PCA+SVM、HOG+CNN等）

检测器实现检测图像中的人脸以及位置，分类器实现将标定后的人脸转换成特征值，最后计算欧氏距离辨别人脸（个别方法除外）。

人脸识别同时还附带其他功能：人脸对齐、人脸匹配（类似识别）

### 1.1 人脸识别流程

类似于上面的步骤，人脸识别的流程为：人脸检测===》特征提取===》人脸识别

其中，人脸检测是使用检测器对图像滑动窗口中的分类和定位，确定滑窗内是不是人脸，以及检测框的具体位置。

特征提取是即提取特征向量，蕴含着几何特征或者表征特征。几何特征：如眼、鼻等几何关系，特点是直观，量小。表征特征：算法提取的局部或者全局特征。

人脸识别：包括确认、辨识。

### 1.2 人脸识别（检测）常用算法

人脸识别算法多种多样，大致分为：

- 基于肤色模型的方法；
- 基于模板匹配的方法；
- 基于弹性模型的方法（典型：DLA）；
- 基于特征脸法（典型：基于KL变换、Eigenface或PCA）；
- 基于神经网络法（典型：RBFN，最新：CNN）；
- 支持向量机法（典型：HOG+SVM，广泛应用于各种检测）；
- 基于积分图像特征发（典型：addboost，广泛应用的方法）；
- 基于概率模型法；
- 基于核技术方法（典型：PCA和LDA、KFDA）。

可以理解为：算法多，没有统一的分类，根据不同的准则可以划分为不同的类（还会有交叉），因此在学习人脸识别的时候，可以从经典算法开始，到最新的卷积神经网络。可以针对自己的需求去专门学习一些算法，不需要全部都学习。

- 经典的人脸识别算法：特征脸法（Eigenface）、局部二值模式（Local Binary Patterns，LBP）、Fisherface
- 常用人脸识别算法：AdaBoost（即Haar+adaboost）、HOG+SVM（此算法也广泛应用到各种目标检测，如行人检测）、PCA+SVM
- 新人脸识别方法：DeepID、MTCNN、PFLD（基本都和深度学习有关）。

### 1.3 haar+adaboost算法

#### 1.3.1 adaboost算法

adaboost算法是Boosting算法的提升版，Boosting方法又起源于PCA学习模型。1984年Valiant提出PCA模型，1990年，SChapire就首先构造出一种多项式级的算法，将弱学习算法提升为强学习算法，就是最初的Boosting算法。1993年，Drucker和Schapire首次以神经网络作为弱学习器，利用Boosting算法解决实际问题。1994年，Kearns和Valiant证明，在Valiant的PAC（Probably ApproximatelyCorrect）模型中，只要数据足够多，就可以将弱学习算法通过集成的方式提高到任意精度。1995年，Freund在Kearns和Valiant证明的基础上提出了一种效率更高的Boosting算法，即现在的Adaboost模型，对Boosting算法有巨大提升。

PCA（Probably Approximately Correct）模型是计算学习理论中常用的模型，PAC学习的实质就是在样本训练的基础上，使算法的输出以概率接近未知的目标概念。PAC学习模型是考虑样本复杂度（指学习器收敛到成功假设时至少所需的训练样本数）及计算复杂度（指学习器收敛到成功假设时所需的计算量）的一个基本框架，成功的学习被定义为形式化的概率理论。

Boosting 原意为提升、加强，现在一般指的是将弱学习算法提升为强学习算法的一类算法。boosting模型就是学习一系列的分类器，每个分类器对其前一个分类器产生的错误给予更大的重视，并增加导致错误分类的样本权值，重新对该样本训练分类器后，再学习下一个分类器。该训练过程重复到设定的次数后停止，最终分类器从这一系列的分类器中得出。Boosting是一种把若干个若分类器结合到一个强分类器中，从而大大提高检测性能的方法。强分类器对数据进行分类，是通过弱分类器的多数投票机制进行的。

Adaboost（Adaptive Boosting）是Boosting家族的代表算法之一，不需要任何关于弱学习器性能的先验知识，而且和Boosting算法具有同样的效率，所以在提出之后得到了广泛的应用。

Adaboost算法已被证明是一种有效而实用的Boosting算法，其算法原理是通过调整样本权重和弱分类器权值，从训练出的弱分类器中筛选出权值系数最小的弱分类器组合成一个最终强分类器。基于训练集训练弱分类器，每次下一个弱分类器都是在样本的不同权值集上训练获得的。每个样本被分类的难易度决定权重，而分类的难易度是经过前面步骤中的分类器的输出估计得到的。

Adaboost算法在样本训练集使用过程中，对其中的关键分类特征集进行多次挑选，逐步训练分量弱分类器，用适当的阈值选择最佳弱分类器，最后将每次迭代训练选出的最佳弱分类器构建为强分类器。其中，级联分类器的设计模式为在尽量保证感兴趣图像输出率的同时，减少非感兴趣图像的输出率，随着迭代次数不断增加，所有的非感兴趣图像样本都不能通过，而感兴趣样本始终保持尽可能通过为止。

首先需要了解若分类器、强分类器和级联分类器。弱分类器：没有专门的定义，可以理解为对分类的效果较弱，正确率较低。强分类器：即分类学习正确率较高，有显著的分类分类效果。级联分类器：将多个分类器连在一起组成一个强分类器，再由多个强分类器级联在一起组成一个标准分类器。

Adaboost是一种基于级联分类模型的分类器，级联分类模型可以用下图表示：

![7](./src/images/figure_7.jpeg)

所有样本进入第一个强分类器，若为正例则进入下一个强分类器，否则，直接作为负例输出，以此类推，直到最后的强分类器输出正例，作为最终的输出结果。

有些强分类器可能包含10个弱分类器，有些则包含20个弱分类器，一般情况下一个级联用的强分类器包含20个左右的弱分类器，然后在将10个强分类器级联起来，就构成了一个级联强分类器，这个级联强分类器中总共包括200弱分类器。因为每一个强分类器对负样本的判别准确度非常高，所以一旦发现检测到的目标位负样本，就不在继续调用下面的强分类器，减少了很多的检测时间。因为一幅图像中待检测的区域很多都是负样本，这样由级联分类器在分类器的初期就抛弃了很多负样本的复杂检测，所以级联分类器的速度是非常快的；只有正样本才会送到下一个强分类器进行再次检验，这样就保证了最后输出的正样本的伪正(false positive)的可能性非常低。

级联结构分类器由多个弱分类器组成，每一级都比前一级复杂。每个分类器可以让几乎所有的正例通过，同时滤除大部分负例。这样每一级的待检测正例就比前一级少，排除了大量的非检测目标，可大大提高检测速度。

其次，Adaboost是一种迭代算法。初始时，所有训练样本的权重都被设为$1/N$，在此样本分布下训练出一个弱分类器。在第（$n=1,2,3,....,T$，T为迭代次数）次迭代中，样本的权重由第n-1次迭代的结果而定。在每次迭代的最后，都有一个调整权重的过程，被分类错误的样本将得到更高的权重。这样分错的样本就被突出出来，得到一个新的样本分布。在新的样本分布下，再次对弱分类器进行训练，得到下一个新的弱分类器。经过T次循环，得到T个弱分类器，把这T个弱分类器按照一定的权重叠加起来，就得到最终的强分类器。

Aadboost 算法系统具有较高的检测速率，且不易出现过适应现象。但是该算法在实现过程中为取得更高的检测精度则需要较大的训练样本集，在每次迭代过程中，训练一个弱分类器则对应该样本集中的每一个样本，每个样本具有很多特征，因此从庞大的特征中训练得到最优弱分类器的计算量增大。

#### 1.3.2 Haar特征

Harr-like特征是Viola等提出的一种简单矩形特征，因其类似于Harr小波而得名，它反映了图像局部的灰度化。影响AdaBoost检测训练算法速度很重要的两方面是特征的选取和特征值的计算。脸部的一些特征可以由矩形特征（特征模板）简单地描绘。如下图示范：

![8](./src/images/figure_8.jpeg)

上图中两个矩形特征，表示出人脸的某些特征。比如中间一幅表示眼睛区域的颜色比脸颊区域的颜色深，右边一幅表示鼻梁两侧比鼻梁的颜色要深。同样，其他目标，如眼睛等，也可以用一些矩形特征来表示。在给定有限的数据情况下，基于特征的检测能够编码特定区域的状态，而且基于特征的系统比基于像素的系统要快得多。矩形特征对一些简单的图形结构，比如边缘、线段，比较敏感，但是其只能描述特定走向（水平、垂直、对角）的结构，因此比较粗略。如上图，脸部一些特征能够由矩形特征简单地描绘，例如，通常眼睛要比脸颊颜色更深；鼻梁两侧要比鼻梁颜色要深；嘴巴要比周围颜色更深。

对于一个 24×24 检测器，其内的矩形模板决定的矩形特征数量超过160,000个，必须通过特定算法甄选合适的矩形特征，并将其组合成强分类器才能检测人脸。

常用的矩形特征有三种：两矩形特征、三矩形特征、四矩形特征，如图：

![9](./src/images/figure_9.png)

由图表可以看出，两矩形特征反映的是边缘特征，三矩形特征反映的是线性特征、四矩形特征反映的是特定方向特征。LienhartR．等对Haar-like矩形特征库作了进一步扩展，加入了旋转$45^o$角的矩形特征。扩展后的特征大致分为4种类型：边缘特征、线特征环、中心环绕特征和对角线特征：如下图

![10](./src/images/figure_10.png)

特征模板的特征值定义为：模板内白色矩形像素和减去黑色矩形像素和$(S_w_h_i_t_e-S_b_l_a_c_k)$。

#### 1.3.3 使用Opencv实现人脸检测

OpenCV自带的AdaBoost程序能够根据用户输入的正样本集与负样本集训练分类器，常用于人脸检测，行人检测等。它的默认特征采用了Haar，不支持其它特征。这次实现的是人脸检测，首先是需要安装opencv库，可以从此下载opencv官方，安装。找到opencv\sources\data\haarcascades中的haarcascade_frontalface_alt.xml文件，此文件是opencv中已经训练好的人脸adaboost联级分类器，只需要解析此文件即可对图像进行检测是否存在人脸，以及检测框的位置。可以将此文件复制到项目路径下。导入分类器的方法是使用CascaClassifter函数，即cv2.CascadeClassifier('./haarcascade_frontalface_alt.xml')

使用face_Cascade.detectMultiScale(image,scaleFactor,minNeighbors,flags,minSize,maxSize)进行识别，例：

faceRects = face_Cascade.detectMultiScale(image, 1.05, 2, cv2.CASCADE_SCALE_IMAGE, minSize_1)

- image：是待检测图像

- scaleFactor：前后两次相继的扫描中，搜索窗口的比例系数

- minNeighbors：构成检测目标的相邻矩形的最小个数。

- minSize：目标的最小尺寸

- maxSize：目标的最大尺寸

下面是使用haar+adaboost实现的人脸识别案例：

先上效果：

![](./src/images/figure_12.png)

再上代码部分：

```python
# coding:utf-8
import cv2
import numpy as np

[x, y, w, h] = [0, 0, 0, 0]

path = './face_images/guys.jpeg'

face_Cascade = cv2.CascadeClassifier("./haarcascades/haarcascade_frontalface_alt.xml")

frame = cv2.imread(path)

size = frame.shape[:2]
image = np.zeros(size, dtype=np.float32)
image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

# 直方图均衡
image = cv2.equalizeHist(image)
im_h, im_w = size
minSize_1 = (im_w // 10, im_h // 10)
faceRects = face_Cascade.detectMultiScale(image, 1.05, 2, cv2.CASCADE_SCALE_IMAGE, minSize_1)
if len(faceRects) > 0:
    for faceRect in faceRects:
        x, y, w, h = faceRect
        cv2.rectangle(frame, (x, y), (x + w, y + h), [255, 255, 0], 2)

cv2.imshow("detection", frame)
cv2.waitKey(0)
print('okay')
```

同时还可以改造成视频内的人脸检测，如下：

```python
# coding:utf-8
import cv2
import numpy as np

[x, y, w, h] = [0, 0, 0, 0]

#video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

face_Cascade = cv2.CascadeClassifier("./haarcascades/haarcascade_frontalface_alt.xml")

cv2.namedWindow("Face Detection System")

while video_capture.isOpened():
    ret, frame = video_capture.read()

    if not ret:
        break

    size = frame.shape[:2]
    image = np.zeros(size, dtype=np.float32)
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # 直方图均衡
    image = cv2.equalizeHist(image)
    im_h, im_w = size
    minSize_1 = (im_w // 8, im_h // 8)
    faceRects = face_Cascade.detectMultiScale(image, 1.05, 2, cv2.CASCADE_SCALE_IMAGE, minSize_1)
    if len(faceRects) > 0:
        for faceRect in faceRects:
            x, y, w, h = faceRect
            cv2.rectangle(frame, (x, y), (x + w, y + h), [0, 255, 0], 2)

    cv2.imshow("Face Detection System", frame)
    key = cv2.waitKey(5)
    if key == int(30):
        break

video_capture.release()
cv2.destroyWindow("Face Detection System")
```
常用的人脸识别库主要有：opencv库、dlib库和face_recognition库等。

dlib由C++编写，可以应用在图相处理、机器学习等领域，详细项目文档和API参考见dlib官网：http://dlib.net/

## 2. dlib安装方法

```bash
sudo apt-get install build-essential cmake -y
sudo apt-get install libgtk-3-dev -y
sudo apt-get install libboost-all-dev -y
pip install dlib
pip install opencv-python
pip install imutils
```

## 3. 使用 dlib 实现人脸识别

dlib是不错的人脸检测、识别的库，检测精度为99.13%。

```bash
pip install pillow
```

- face_images 保存多张人多脸的图片，供学习使用

- model 保存已经下载好的模型数据

- people_i_know 保存已经认识的人的单张图片

- unknow_people 保存需要识别的人的图像

- video 保存检测和识别使用的视频

### 3.1 图像检测人脸

检测人脸，实现的是使用检测框将图像中的人脸检测出，使用的是dlib与PIL实现，保存在test1.py中。代码和检测结果如下：

```python
import dlib
from PIL import Image
import numpy as py
import os

file_path = "./people_i_know/huge.jpg"

detector = dlib.get_frontal_face_detector()  # 加载检测器
win = dlib.image_window()

image = py.array(Image.open(file_path))  # 读取图片
detector_face = detector(image, 1)  # 检测 1是放大图片

win.clear_overlay()
win.set_image(image)  # 显示图pain
win.add_overlay(detector_face)  # 添加检测框
dlib.hit_enter_to_continue()
```

![1](./src/images/figure_1.png)

### 3.2 检测视频中的人脸

在检测人脸的基础上，融合opencv，实现视频中的人脸检测，保存在test2.py中，代码和结果如下：

```python
import dlib
import cv2
import numpy as np

# video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

detector = dlib.get_frontal_face_detector()
win = dlib.image_window()

while video_capture.isOpened():
    # Grab a single frame of video
    ret, frame = video_capture.read()  # 逐帧读取图片

    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    frame = frame[:, :, ::-1]  # BGR格式转成RGB格式

    image = np.array(frame)
    detector_face = detector(image, 1)
    win.clear_overlay()
    win.set_image(image)
    win.add_overlay(detector_face)

video_capture.release()
cv2.destroyAllWindows()
```

![2](./src/images/figure_2.png)

### 3.3 检测人脸的阈值设置

以上检测人脸使用的都是默认的阈值，在实际练习的时候，可以按照自己的需求阈值。可以简单的认为检测人脸时，都会有一个得分，当得分高于阈值时，将其输出。`dets, sco, idx = detector.run(image,0,1)`为阈值设置部分，`dets`是检测框，`sco`是检测到的人脸得分，`idx`是检测器类型，1即为人为设定的阈值。通过设置不同的阈值，你会发现检测框会很多，或者没有，或者吧人脸完美框住。

保存在test3.py中，代码和结果如下：

```python
import dlib
import numpy as py
from PIL import Image

path = "./people_i_know/figure_1.jpg"
detector = dlib.get_frontal_face_detector()
win = dlib.image_window()

image = py.array(Image.open(path))
dets, sco, idx = detector.run(image, 0, 0.5)  # 设置阈值
for i, d in enumerate(dets):
    print('%d：score %f, face_type %f' % (i, sco[i], idx[i]))

win.clear_overlay()
win.set_image(image)
win.add_overlay(dets)
dlib.hit_enter_to_continue()
```

![3](./src/images/figure_1.png)

### 3.4 实现人脸识别/人脸对比

识别人脸需要人脸特征向量，通过比对两张脸之间特征向量的距离来判定，需要检测的人是否认识，以及这个人叫什么。因此，这里引入了shape_predictor_68_face_landmarks.dat与dlib_face_recognition_resnet_model_v1.dat，前者是人脸68点的检测模型，后者是人脸特征向量模型，都是已经训练好的。这些模型数据都已经放在资源中，也可以自行去官网下载后解压：http://dlib.net/files/

同时还引入了距离函数，代码中简化为 Eu(a,b)。具体代码保存在test4.py中，代码和结果如下：

```python
import dlib
from PIL import Image
import numpy as np
import os

detector = dlib.get_frontal_face_detector()

path_pre = "./model/shape_predictor_68_face_landmarks.dat"  # 68点模型
pre = dlib.shape_predictor(path_pre)

path_model = "./model/dlib_face_recognition_resnet_model_v1.dat"  # resent模型
model = dlib.face_recognition_model_v1(path_model)

path_know = "./people_i_know"  # 已知人脸文件夹
path_unknow = "./unknow_people"  # 需要检测的人来你文件夹

know_list = {}


def Eu(a, b):  # 距离函数
    return np.linalg.norm(np.array(a) - np.array(b), ord=2)


for path in os.listdir(path_know):  # 载入已知人的名字和面部特征
    img = np.array(Image.open(path_know + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    know_encode = model.compute_face_descriptor(img, shape)
    know_list[name] = know_encode

match = {}
for path in os.listdir(path_unknow):  # 逐个载入待检测人的面部特征
    img = np.array(Image.open(path_unknow + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    unknow_encode = model.compute_face_descriptor(img, shape)
    match[name] = "unknow"

    for key, sco in know_list.items():  # 与已知的面部特征匹配
        if Eu(unknow_encode, sco) < 0.6:
            match[name] = key
            break

for key, value in match.items():
    print("picture:" + key + ' is ' + value)

```

结果：

```bash
./test4.py
picture:unknown_1 is biden
picture:unknown_4 is unknow
picture:unknown_2 is unknow
picture:unknown_3 is unknow

Process finished with exit code 0
```

代码设计3个文件夹，

model 文件夹存放的是上述的两个模型数据

people_i_know 文件夹存放的是你事先知道的人，以及他的样子。

unknow_people 文件夹存放的是你想识别的人，看这个人是否是你认识的（显示他的名字），或者是你不认识的（显示unknow）

从下图中可以看出，unknown_1文件是biden，其它文件是不认识的人。

### 3.5 ROI追踪（感兴趣区域追踪）

感兴趣区域，是一副图像中，我们最关心的地方。感兴趣区域追踪，是指检测框将随着这个物体的移动而随之移动，始终将物体的位置检测出。

下列代码实现的是对视频中感兴趣区域的检测，代码一运行需要人为的使用鼠框选选自己感兴趣的区域，并按下enter确认，过后，检测框将自动追踪物体或者人。

保存在test5.py中，代码和结果如下：

```python
import dlib
import cv2

#video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

detector = dlib.correlation_tracker()
win = dlib.image_window()

i = 0
while video_capture.isOpened():
    # Grab a single frame of video
    ret, frame = video_capture.read()

    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    frame = frame[:, :, ::-1]

    if i == 0:  # 在第一帧的时候调用鼠标回去检测框的位置
        cv2.namedWindow('image', flags=cv2.WINDOW_NORMAL)
        cv2.imshow('image', frame)
        rect = cv2.selectROI('image', frame, showCrosshair=False, fromCenter=False)
        cv2.destroyAllWindows()
        detector.start_track(frame, dlib.rectangle(rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3]))
        i = i + 1
    else:  # 继续追踪内容
        detector.update(frame)

    win.clear_overlay()
    win.set_image(frame)
    win.add_overlay(detector.get_position())

video_capture.release()
cv2.destroyAllWindows()
```

下图蓝色框是人为使用鼠标框选的位置。

下图红色框是自动追踪的位置。由于视频中的摄像机经常切换位置，因此检测结果不是很理想。

可以知道：当场景的角度位置变化不大的时候，检测结果较理想；当场景切换过大，可能会出现误检测的现象。

![2](./src/images/figure_2.png)

- 图像中多张人脸识别

现实生活中，一张图往往有许多的人脸。下列代码重点就是将一幅图中认识的人脸识别出来，对不认识的人不检测。

检测使用的是dlib，绘图部分使用的是opencv。保存在test6.py中，代码和结果如下：

```python
import dlib
from PIL import Image
import numpy as np
import os
import cv2
from imutils import face_utils

detector = dlib.get_frontal_face_detector()

path_pre = "./model/shape_predictor_68_face_landmarks.dat"
pre = dlib.shape_predictor(path_pre)

path_model = "./model/dlib_face_recognition_resnet_model_v1.dat"
model = dlib.face_recognition_model_v1(path_model)

path_know = "./people_i_know"

path_unknow = "./face_images/bald_guys.jpg"

know_list = {}


def Eu(a, b):
    return np.linalg.norm(np.array(a) - np.array(b), ord=2)


for path in os.listdir(path_know):  # 建立已知人脸的字典库
    img = np.array(Image.open(path_know + "/" + path))
    name = path.split('.')[0]
    det_img = detector(img, 0)
    shape = pre(img, det_img[0])
    know_encode = model.compute_face_descriptor(img, shape)
    know_list[name] = know_encode

frame = cv2.imread(path_unknow)
# frame = frame[:, :, ::-1]
img = np.array(frame)

det_img = detector(img, 0)  # 得到检测框

for rect in enumerate(det_img):
    shape = pre(img, rect[1])
    (x, y, w, h) = face_utils.rect_to_bb(rect[1])  # rect由tuple转成bb
    unknow_encode = model.compute_face_descriptor(img, shape)
    for key, sco in know_list.items():
        if Eu(unknow_encode, sco) < 0.6:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 3)  # 绘制检测框
            cv2.putText(img, key, (x, y + h), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)  # 绘制检测后的姓名
            break

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('output', img)
cv2.waitKey(0)

```
检测的图像中包含许多张人脸，已经检测c出的人：obama和biden。从结果中，可以看到也有漏检测和检测错误的情况，原因在于，people_i_know文件夹下的obama和biden是最新美化过的图像。

![4](./src/images/figure_4.png)

dlib的检测精度为99.13%，这次使用的face_recognition库的检测精度为99.38%，二者都是很好的人脸识别库，都支持机器学习等方面的应用。face_recognition除了在程序中使用外，也可以在命令窗内直接使用。

## 4. 安装face_recognition库

安装库之前需要安装dlib库，因为face_recognition是以dlib为支撑。安装完dlib之后，进入face_recognition_master文件夹内，打开命令窗口，进入所需要的环境，输入：

```bash
pip install face_recognition
```

### 4.1 人脸检测和人脸轮廓检测

使用检测框框取人脸部分和绘出人脸轮廓必要的68点。具体代码保存在test7.py中，效果如下：

```python
import face_recognition
import cv2

path = './face_images/guys.jpeg'

img = cv2.imread(path)
img_rgb = img[:, :, ::-1]

face_location = face_recognition.face_locations(img_rgb)  # 检测框位置
face_landmarks_list = face_recognition.face_landmarks(img_rgb)  # 面部轮廓位置

for i in range(len(face_location)):  # 绘制检测狂
    rect = face_location[i]
    cv2.rectangle(img, (rect[3], rect[0]), (rect[1], rect[2]), (0, 0, 255), 2)

for word, face_landmarks in enumerate(face_landmarks_list):  # 绘制面部轮廓点
    for key, marks in face_landmarks.items():
        for i in range(len(marks)):
            point = marks[i]
            cv2.circle(img, (point[0], point[1]), 2, (0, 255, 0))
            # img[point[1], point[0]]= [255,255,255]

cv2.imshow('img', img)
cv2.waitKey(0)
print("finish")
```

### 4.2 人脸比对

实现两张人脸的比对，如果是同一人，输出true，否则输出false。具体代码保存在test8.py中，效果如下：
```python
import face_recognition

path_know = './people_i_know/obama.jpeg'
path_unknow = './unknow_people/unknown_5.jpg'

know = face_recognition.load_image_file(path_know)  # 已知人的面部轮廓位置
unknow = face_recognition.load_image_file(path_unknow)  # 未知人的面部轮廓位置

know_encoding = face_recognition.face_encodings(know)[0]  # 已知人面部编码
unknow_encoding = face_recognition.face_encodings(unknow)[0]  # 未知人面部编码

result = face_recognition.compare_faces([know_encoding], unknow_encoding)  # 对比两个面部编码
print(result)
```

输出
```bash
test8.py
[True]
```

- 检测视频中的人脸

在检检测人脸的基础上，结合opencv实现在视频流中的人脸识别功能。具体代码保存在test9.py中，效果如下：

```python
import face_recognition
import cv2

#video_capture = cv2.VideoCapture('./video/demo_clip.mp4')
video_capture = cv2.VideoCapture(0)

while True:
    ret, frame = video_capture.read()
    img_rgb = frame[:, :, ::-1]

    face_location = face_recognition.face_locations(img_rgb)
    # face_location = face_recognition.face_locations(img_rgb, model="cnn")# 使用CNN模型

    for (top, right, bottom, left) in face_location:
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)  # 绘制检测框
    cv2.imshow('image', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()
```

![5](./src/images/figure_5.png)

- 人脸识别

检测人脸之后，通过对 面部特征的编码，将其转化为特征向量，通过比对两张脸之间的特征向量确定人脸是否认识。

其中path_know是已经认识人的文件路径，`path_unknow`是需要识别的文件路径。这就需要事先把认识的人和不认识的人分别放在`people_i_know`和`unknow_people`文件夹中。

程序输出的是需要识别的人的具体名字或者unknow，具体的代码保存在test10.py中，效果如下：

```python
import face_recognition
import os

path_know = './people_i_know'
path_unknow = './unknow_people'

know = {}
for path in os.listdir(path_know):  # 建立已知人的面部特征字典库
    img = face_recognition.load_image_file(path_know + '/' + path)
    encoding = face_recognition.face_encodings(img)[0]
    name = path.split('.')[0]
    know[name] = encoding

match = {}
for path in os.listdir(path_unknow):
    img = face_recognition.load_image_file(path_unknow + '/' + path)
    encoding = face_recognition.face_encodings(img)[0]  # 获取未知人的面部特征
    name = path.split('.')[0]
    match[name] = 'unknow'
    for key, value in know.items():
        if face_recognition.compare_faces([value], encoding)[0]:  # 与库里的特征进行比对
            match[name] = key
            break

print(match)

for key, value in match.items():
    print(key + ' is ' + value)
```

运行输出

```bash
test10.py
{'unknown_1': 'biden', 'unknown_5': 'obama', 'unknown_4': 'unknow', 'unknown_2': 'unknow', 'unknown_3': 'unknow'}
unknown_1 is biden
unknown_5 is obama
unknown_4 is unknow
unknown_2 is unknow
unknown_3 is unknow

Process finished with exit code 0
```

- 视频中人脸识别

在人脸识别之上，结合opencv实现视频中的人脸识别。具体代码保存在test5.py中，效果如下：

```python
import face_recognition
import cv2
import numpy as np
import os
# coding=utf-8

video_capture = cv2.VideoCapture(0)
#video_capture = cv2.VideoCapture('./video/demo_clip.mp4')

path_know = './people_i_know'

known_face_encodings = []
known_face_names = []

for path in os.listdir(path_know):
    img = face_recognition.load_image_file(path_know + '/' + path)
    encoding = face_recognition.face_encodings(img)[0]
    known_face_encodings.append(encoding)
    name = path.split('.')[0]
    known_face_names.append(name)

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Unknown"

            # # If a match was found in known_face_encodings, just use the first one.
            # if True in matches:
            #     first_match_index = matches.index(True)
            #     name = known_face_names[first_match_index]

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
```

 ![6](./src/images/figure_6.png)



- CNN模型与CUDA加速人脸检测(需要gGPU支持CUDA)

使用CNN模型批量检测视频、图片中的人脸，通过使用GPU加速，速度是原先的3倍左右。程序结果不输出图像，仅仅输出文字结果。具体代码保存在test12.py中，效果如下：

```python
import face_recognition
import cv2

# Open video file
# video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

frames = []
frame_count = 0

while video_capture.isOpened():
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Bail out when the video file ends
    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    frame = frame[:, :, ::-1]

    # Save each frame of the video to a list
    frame_count += 1
    frames.append(frame)

    # Every 128 frames (the default batch size), batch process the list of frames to find faces
    if len(frames) == 128:
        batch_of_face_locations = face_recognition.batch_face_locations(frames, number_of_times_to_upsample=0)

        # Now let's list all the faces we found in all 128 frames
        for frame_number_in_batch, face_locations in enumerate(batch_of_face_locations):
            number_of_faces_in_frame = len(face_locations)

            frame_number = frame_count - 128 + frame_number_in_batch
            print("I found {} face(s) in frame #{}.".format(number_of_faces_in_frame, frame_number))

            for face_location in face_locations:
                # Print the location of each face in this frame
                top, right, bottom, left = face_location
                print(
                    " - A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left,
                                                                                                             bottom,
                                                                                                             right))

        # Clear the frames array to start the next batch
        frames = []
```

至此，我们已经可以使用dlib 或者 face_recognition 库实现人脸检测、识别，还推广到视频和其他的场景中。这些都是为学习人脸识别准备，如果有人脸识别的理论基础最好，若是没有理论基础，可以先跟随以上代码抄一抄、调一调参数，找一找感觉。
