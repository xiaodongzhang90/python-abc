import face_recognition
import os

path_know = './people_i_know'
path_unknow = './unknow_people'

know = {}
for path in os.listdir(path_know):  # 建立已知人的面部特征字典库
    img = face_recognition.load_image_file(path_know + '/' + path)
    encoding = face_recognition.face_encodings(img)[0]
    name = path.split('.')[0]
    know[name] = encoding

match = {}
for path in os.listdir(path_unknow):
    img = face_recognition.load_image_file(path_unknow + '/' + path)
    encoding = face_recognition.face_encodings(img)[0]  # 获取未知人的面部特征
    name = path.split('.')[0]
    match[name] = 'unknow'
    for key, value in know.items():
        if face_recognition.compare_faces([value], encoding)[0]:  # 与库里的特征进行比对
            match[name] = key
            break

print(match)

for key, value in match.items():
    print(key + ' is ' + value)