import face_recognition

path_know = './people_i_know/obama.jpeg'
path_unknow = './unknow_people/unknown_5.jpg'

know = face_recognition.load_image_file(path_know)  # 已知人的面部轮廓位置
unknow = face_recognition.load_image_file(path_unknow)  # 未知人的面部轮廓位置

know_encoding = face_recognition.face_encodings(know)[0]  # 已知人面部编码
unknow_encoding = face_recognition.face_encodings(unknow)[0]  # 未知人面部编码

result = face_recognition.compare_faces([know_encoding], unknow_encoding)  # 对比两个面部编码
print(result)