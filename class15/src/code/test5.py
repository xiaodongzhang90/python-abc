import dlib
import cv2

#video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

detector = dlib.correlation_tracker()
win = dlib.image_window()

i = 0
while video_capture.isOpened():
    # Grab a single frame of video
    ret, frame = video_capture.read()

    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    frame = frame[:, :, ::-1]

    if i == 0:  # 在第一帧的时候调用鼠标回去检测框的位置
        cv2.namedWindow('image', flags=cv2.WINDOW_NORMAL)
        cv2.imshow('image', frame)
        rect = cv2.selectROI('image', frame, showCrosshair=False, fromCenter=False)
        cv2.destroyAllWindows()
        detector.start_track(frame, dlib.rectangle(rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3]))
        i = i + 1
    else:  # 继续追踪内容
        detector.update(frame)

    win.clear_overlay()
    win.set_image(frame)
    win.add_overlay(detector.get_position())

video_capture.release()
cv2.destroyAllWindows()